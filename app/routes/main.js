var dbConnection = require('../../config/database');

module.exports = function(app){    
  app.get('/', function(req, res){
    var connection = dbConnection();
    connection.query('select * from mainbg_pics', function(error, result){
      res.render("main/index.ejs", {bgpics: result});
    });
  });

  app.get('/about', function(req, res){
    res.render("main/about");
  });

  app.get('/gallery', function(req, res){
    res.render("main/gallery");
  });

  app.get('/projects', function(req, res){
    res.render("main/projects");
  });

  app.get('/contact', function(req, res){
    res.render("main/contact");
  });
}