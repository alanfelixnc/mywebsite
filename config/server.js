var cool = require('cool-ascii-faces');
var express = require('express');
var app = express();

//define a porta
app.set('port', (process.env.PORT || 5000));

//define views
app.set('view engine', 'ejs');
app.set('views', './app/views');

//define diretório padrão
app.use(express.static('./public'));

module.exports = app;