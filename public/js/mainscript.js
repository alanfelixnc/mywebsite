function loadContent(contentId) {
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {
		if(this.status == 200 && this.readyState == 4) {
			var content = document.getElementById("content-info");
			content.innerHTML = this.responseText;
		}
	}
	xhr.open("GET", contentId, true);
	xhr.send();
}

function showContent(){
	var main = document.getElementById("main");
	main.classList.add("hidden");
	var sub = document.getElementById("content-info");
	sub.classList.add("show");
	var anchs = document.getElementsByTagName("nav");
	anchs = anchs[0];
	anchs.classList.add("top-nav");
	anchs = anchs.children;
	for(i = 0; i < anchs.length; i++){
		if(anchs[i].classList.contains("back")){
			anchs[i].classList.remove("back");
		}
	}
	loadContent(this.id);
}

function showMain(){
	var content = document.getElementById("content-info");
	content.classList.remove("show");
	content.innerHTML = "";
	var anchs = document.getElementsByTagName("nav");
	anchs = anchs[0];
	anchs.classList.remove("top-nav");
	anchs = anchs.children;
	var back = anchs[anchs.length-1];
	back.classList.add("back");
	var main = document.getElementById("main");
	main.classList.remove("hidden");
}

var anchs = document.getElementsByTagName("nav");
anchs = anchs[0].children;
for(i = 0; i < anchs.length; i++){
	if(anchs[i].classList.contains("back")){
		anchs[i].addEventListener('click', showMain);
	} else {
		anchs[i].addEventListener('click', showContent);
	}
function showSection(){
    var main = document.getElementById("main");
    main.classList.add("hidden");
    var sub = document.getElementById("about");
    sub.classList.add("show");
    var anchs = document.getElementsByTagName("nav");
    anchs = anchs[0];
    anchs.classList.add("top-nav");
    anchs = anchs.children;
    for(i = 0; i < anchs.length; i++){
        if(anchs[i].classList.contains("back")){
            anchs[i].classList.remove("back");
        }
    }
}

function sectionBack(){
    var sections = document.getElementsByClassName("sub");
    var anchs = document.getElementsByTagName("nav");
    anchs = anchs[0];
    anchs.classList.remove("top-nav");
    anchs = anchs.children;
    var back = anchs[anchs.length-1];
    var main = document.getElementById("main");
    var active = "";
    for(i = 0; i < sections.length; i++){
        if(sections[i].classList.contains("show")){
            active = sections[i];
            active.classList.remove("show");
            main.classList.remove("hidden");
            back.classList.add("back");
        }
    }
}

var anchs = document.getElementsByTagName("nav");
anchs = anchs[0].children;
for(i = 0; i < anchs.length; i++){
    if(anchs[i].classList.contains("back")){
        anchs[i].addEventListener('click', sectionBack);
    } else {
        anchs[i].addEventListener('click', showSection);
    }
}