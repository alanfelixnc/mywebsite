var app = require('./config/server');

var mainpage = require('./app/routes/main.js')(app);

app.listen(app.get('port'), function(){
    console.log("*****************");
    console.log("Modulo: Express")
    console.log("Status: ON");
    console.log("Porta: " + app.get('port'));
    console.log("*****************");
    console.log("Servidor ligado! :D");
    console.log("\n");
});